---
title: Download
layout: download
# This comes from the id in org.kde.yourapp.appdata.xml
# In the original project
appstream: org.kde.template
name: Template App
gear: true
menu:
  main:
    weight: 2
sources:
  - name: Flatpak
    # reusable-assets comes from the aether-hugo theme
    # https://invent.kde.org/websites/aether-sass/-/tree/hugo/static/reusable-assets
    src_icon: /reusable-assets/flatpak.png
    description: |
      **Add flathub repository using:**

      ```bash
      flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
      ```

      **Install Template App**

      ```bash
      flatpak install flathub org.kde.template
      ```
  - name: Windows
    src_icon: /reusable-assets/windows.svg
    description: |
      * [Microsoft store](https://www.microsoft.com/en-us/p/elisa/9pb5md7zh8tl)
      * [Win64 executable](https://binary-factory.kde.org/view/Windows%2064-bit/job/Elisa_Release_win64/)
---
