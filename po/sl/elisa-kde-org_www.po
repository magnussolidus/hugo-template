#
# Matjaž Jeran <matjaz.jeran@amis.net>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: 1.0\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-01-05 00:52+0000\n"
"PO-Revision-Date: 2022-02-16 09:08+0100\n"
"Last-Translator: Matjaž Jeran <matjaz.jeran@amis.net>\n"
"Language-Team: English <lugos-slo@lugos.si>\n"
"Language: sl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 21.12.2\n"
"Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n"
"%100==4 ? 3 : 0);\n"

#: config.yaml:0 content/_index.md:0 content/download.md:0
#: content/get-involved.md:0 content/users.md:0 i18n/en.yaml:0
msgid "Elisa"
msgstr "Elisa"

#: config.yaml:0
msgid "A music player that is simple, reliable, and a joy to use."
msgstr ""
"Predvajalnik glasbe, ki je preprost, zanesljiv in ga boste z veseljem "
"uporabljali."

#: config.yaml:0
msgid "Donate"
msgstr "Donirajte"

#: content/download.md:0 i18n/en.yaml:0
msgid "Windows"
msgstr "Windows"

#: content/download.md:0
msgid "Download"
msgstr "Prenesi"

#: content/download.md:0
msgid "Flatpak"
msgstr "Flatpak"

#: content/download.md:0
msgid ""
"**Add flathub repository using:**\n"
"\n"
"```bash\n"
"flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub."
"flatpakrepo\n"
"```\n"
"\n"
"**Install Elisa**\n"
"\n"
"```bash\n"
"flatpak install flathub org.kde.elisa\n"
"```\n"
msgstr ""
"**Dodaj skladišče flathub z uporabo:**\n"
"\n"
"```bash\n"
"flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub."
"flatpakrepo\n"
"```\n"
"\n"
"**Namesti paket Elisa**\n"
"\n"
"```bash\n"
"flatpak install flathub org.kde.elisa\n"
"```\n"

#: content/download.md:0
msgid ""
"* [Microsoft store](https://www.microsoft.com/en-us/p/elisa/9pb5md7zh8tl)\n"
"* [Win64 executable](https://binary-factory.kde.org/view/Windows%2064-bit/"
"job/Elisa_Release_win64/)"
msgstr ""
"* [Microsoft store](https://www.microsoft.com/en-us/p/elisa/9pb5md7zh8tl)\n"
"* [Win64 executable](https://binary-factory.kde.org/view/Windows%2064-bit/"
"job/Elisa_Release_win64/)"

#: content/get-involved.md:0
msgid "Get Involved"
msgstr "Bodite vpleteni"

#: content/get-involved.md:0
msgid ""
"Most development-related discussions take place on the [Elisa mailing list]"
"(http://mail.kde.org/mailman/listinfo/elisa).\n"
"Just join in, say hi and tell us what you would like to help us with!"
msgstr ""
"Večina debat na temo razvoja se dogaja na poštnems seznamu [Elisa mailing "
"list](http://mail.kde.org/mailman/listinfo/elisa).\n"
"Pridružite se, pozdravite in povejte nam, kako bi nam radi pomagali!"

#: content/get-involved.md:14
msgid ""
"Want to contribute to Elisa? Check out [Phabricator](https://phabricator.kde."
"org/tag/elisa/) for some fun task or [browse the source code](https://invent."
"kde.org/multimedia/elisa)."
msgstr ""
"Bi radi prispevali k Elisi? Preverite [Phabricator](https://phabricator.kde."
"org/tag/elisa/) za kako zabavno nalogo ali [pobrskajte po izvorni kodi]"
"(https://invent.kde.org/multimedia/elisa)."

#: content/users.md:0
msgid "User Support"
msgstr "Podpora uporabnikom"

#: content/users.md:0
msgid "https://docs.kde.org/stable5/en/elisa/elisa/index.html"
msgstr "https://docs.kde.org/stable5/en/elisa/elisa/index.html"

#: i18n/en.yaml:0
msgid "Linux"
msgstr "Linux"

#: i18n/en.yaml:0
msgid "FreeBSD"
msgstr "FreeBSD"

#: i18n/en.yaml:0
msgid "Other ways to download"
msgstr "Ostali načini prenosa"

#: i18n/en.yaml:0
msgid ""
"<b>Elisa</b> is a music player developed by the KDE community that strives "
"to be simple and nice to use. We also recognize that we need a flexible "
"product to account for the different workflows and use-cases of our users. "
"We focus on a very good integration with the Plasma desktop of the KDE "
"community without compromising support for other platforms (other Linux "
"desktop environments, Windows, and Android). We are creating a reliable "
"product that is a joy to use and respects our users' privacy. As such, we "
"prefer to support online services where users are in control of their data."
msgstr ""
"<b>Elisa</b> je predvajalnik glasbe, ki ga je razvila skupnost KDE in si "
"prizadeva biti preprost in prijeten za uporabo. Prav tako se zavedamo, da "
"potrebujemo prilagodljiv izdelek, ki bo upošteval različne poteke dela in "
"primere uporabe naših uporabnikov. Osredotočeni smo na zelo dobro "
"integracijo namizjem Plasma skupnosti KDE, ne da bi ogrozili podporo drugim "
"platformam (druga namizna okolja Linux, Windows in Android). Ustvarjamo "
"zanesljiv izdelek, ki ga je z veseljem uporabljati in spoštuje zasebnost "
"naših uporabnikov. Kot taki raje podpiramo spletne storitve, kjer uporabniki "
"nadzorujejo svoje podatke."

#: i18n/en.yaml:0
msgid "Available on all major platforms"
msgstr "Na voljo na vseh glavnih platformah"

#: i18n/en.yaml:0
msgid "Browse music by album, artist, or tracks"
msgstr "Brskajte po glasbi po albumu, umetniku ali skladbah"

#: i18n/en.yaml:0
msgid ""
"Elisa, powered with Baloo indexing support, automatically looks in your "
"music folder for music files and cover art. The left column has various "
"viewing options: Now Playing, Albums, Artists, and Tracks."
msgstr ""
"Elisa s podporo za indeksiranje Baloo samodejno išče glasbene datoteke in "
"naslovnice v glasbeni mapi. V levem stolpcu so na voljo različne možnosti "
"ogleda: zdaj se predvaja, albumi, izvajalci in skladbe."

#: i18n/en.yaml:0
msgid "Create and manage playlists"
msgstr "Ustvari in upravljaj sezname predvajanj"

#: i18n/en.yaml:0
msgid "Create and manage all your playlists from the built-in side panel."
msgstr ""
"Na vgrajeni stranski plošči ustvarite in upravljajte vse svoje sezname "
"predvajanja."

#: i18n/en.yaml:0
msgid "Screenshot of the playlist feature"
msgstr "Posnetek zaslona funkcije seznama predvajanja"

#: i18n/en.yaml:0
msgid "Party mode"
msgstr "Način zabave"

#: i18n/en.yaml:0
msgid "Turn Party Mode on with Elisa's immersive built-in party mode feature."
msgstr "Vklopite način zabave z Elisino izjemno vgrajeno funkcijo zabave."

#: i18n/en.yaml:0
msgid "Screenshot of Elisa's party mode"
msgstr "Posnetek zaslona Elisinega načina zabave"

#: i18n/en.yaml:0
msgid "Dark / Light Themes"
msgstr "Temne / svetle teme"

#: i18n/en.yaml:0
msgid ""
"Elisa has both a <b>Light</b> and a <b>Dark</b> theme, so you can enjoy your "
"music without the colors bothering you."
msgstr ""
"Elisa ima temo <b>Svetlo</b> in <b>Temno</b>temo, zato lahko uživate v svoji "
"glasbi, ne da bi vas motile barve."

#: i18n/en.yaml:0
msgid "Fast indexing"
msgstr "Hitro indeksiranje"

#: i18n/en.yaml:0
msgid ""
"With the help of <a href=\"https://community.kde.org/Baloo\">Baloo</a>, "
"Elisa is fast to index and sort all your files. It is also possible to use "
"Elisa without Baloo."
msgstr ""
"S pomočjo pograma <a href=\"https://community.kde.org/Baloo\">Baloo</a>, je "
"Elisa hitra pri indeksiranju in razvrščanju vseh vaših datotek. Eliso lahko "
"uporabljate tudi brez programa Baloo."

#: i18n/en.yaml:0
msgid "Adapts to your screen"
msgstr "Se prilagodi vašemu zaslonu"

#: i18n/en.yaml:0
msgid ""
"Elisa uses <a href=\"https://develop.kde.org/frameworks/kirigami/"
"\">Kirigami</a> to be the best convergent, responsive music player that "
"adapts its content to your screen. Soon on your phone!"
msgstr ""
"Elisa uporablja <a href=\"https://develop.kde.org/frameworks/kirigami/"
"\">Kirigami</a> da bi bil najboljši konvergentni, odzivni predvajalnik "
"glasbe, ki svojo vsebino prilagodi zaslonu. Kmalu na vašem telefou!"

#: i18n/en.yaml:0
msgid "Open Source"
msgstr "Odpri vir"

#: i18n/en.yaml:0
msgid ""
"Elisa is Open Source and you can browse, edit and share the source code.  "
"Elisa is <a href=\"https://kde.org\">Made By KDE</a>, a community building "
"high-quality projects that your are free to use. Check out all <a href="
"\"https://kde.org/products\"> our projects!</a>"
msgstr ""
"Elisa je odprtokodna in lahko brskate, urejate in delite izvorno kodo. Eliso "
"je <a href=\"https://kde.org\">Izdelala skupnost KDE</a>, ki gradi visoko-"
"kakovostne projekte, ki jih lahko prosto uporabljate. Oglejte si vse <a href="
"\"https://kde.org/products\">naše projekte!</a>"

#~ msgid "org.kde.elisa.desktop"
#~ msgstr "org.kde.elisa.desktop"

#~ msgid "/assets/img/windows.png"
#~ msgstr "/assets/img/windows.png"

#~ msgid "https://forum.kde.org/"
#~ msgstr "https://forum.kde.org/"

#~ msgid "LANGUAGE_NAME"
#~ msgstr "Slovenščina"
